import React, { Component } from 'react'
import FormInput from './components/FormInput'

 class SignIn extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         submitted:false
      }
    }
    chnageSubmit=(e)=>{
        e.preventDefault()
        this.setState({
            submitted:true
        })
    }
  render() {
    if(!this.state.submitted){
        return (
            <FormInput
            handleSubmit={this.chnageSubmit}
            />
        )
    }
     else{
        return (<h1 style={{textAlign:"center",marginTop:"10%"}}>form submitted succesfully</h1>)
    }
  }
}

export default SignIn