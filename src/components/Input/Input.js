import React from 'react'

export default function Input(props) {
  
  let {type, handleChange, labelName}=props
  return (
    <div className='input'>
        <label>
            <p>{labelName}</p>
           <p><input type={type} onChange={(e)=> handleChange(e)} required/></p>
        </label>
        
    </div>
    
  )
}
