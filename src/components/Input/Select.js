// import React, { useState } from 'react'

function Select(props) {
    
  let {options, labelName,handleChange}=props
    
  return (<div className="input">
       <label>
       <p> {labelName}</p>
       <p><select onChange={e=>{handleChange(e)}} required>
            {options.map((option,idx) => {
                return <option  key={idx}>{option}</option>
            })}
    
    </select>
    </p>
     </label>
  </div>
    
  )
}

export default Select