import React, { Component } from 'react'
import Input from './Input/Input'
import Select from './Input/Select'
export class FormInput extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         name:"",
         collage:"",
         branch:"CSE",
         gender:'Male',
         dob:"",
         email: '',
         password:"",
         confirmPassword:"",
         Mobile:"",
         Address:""
      }
    }
    handleCheck=(e)=>{
      e.preventDefault()
      let formdetails=this.state
      for(let key in formdetails){
        if(formdetails[key]===""){
          alert("some details are missing")
          return 
        }
      }
      if(formdetails.Mobile.length!==10 || isNaN(formdetails.Mobile)){
        alert('enter correct mobile no')
        return
      }
      if(formdetails.password!==formdetails.confirmPassword){
        alert('Mismatching Password')
        return
      }
      this.props.handleSubmit(e)

    }
    handleNameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    handleCollageChange=(e)=>{
        this.setState({
            collage:e.target.value
        })
    }
    handleBranchChange=(e)=>{
        this.setState({
            branch:e.target.value
        })
    }
    handleGenderChange=(e)=>{
        this.setState({
            gender:e.target.value
        })
    }
    handleDOB=(e)=>{
      this.setState({
        dob:e.target.value
      })
    }
    handlePassword=(e)=>{
      this.setState({
        password:e.target.value
      })
    }
    handleMobile=(e)=>{
      this.setState({
        Mobile:e.target.value
      })
    }
    handleConfirmPassword=(e)=>{
      this.setState({
        confirmPassword:e.target.value
      })
    }
    handleAddres=(e)=>{
      this.setState({
        Address:e.target.value
      })
    }
    handleEmail=(e) => {
      this.setState(
        {email: e.target.value}
     )}
    
  render() {
    return (
      <div>
        
        <form>
          <Input type="email" 
          labelName="Email:" 
          handleChange={this.handleEmail} 
          />

       <Input type="text" 
          labelName="Name :" 
          handleChange={this.handleNameChange} 
          />

        <Input type="text" 
          labelName="College :" 
          handleChange={this.handleCollageChange} 
          />
        <Select
        labelName="Branch :"
        options={['CSE','Civil','Mechanical','Electrical']}
        handleChange={this.handleBranchChange}
        />
        <Select
        labelName="Gender :"
        options={['Male','Female','other']}
        handleChange={this.handleGenderChange}
        />

        <Input type="date" labelName="Date of Birth:" handleChange={this.handleDOB}/>
        <Input type="text" labelName="Mobile No :" handleChange={this.handleMobile}/>
        <Input type="password" labelName="Password :" handleChange={this.handlePassword}/>
        <Input type="password" labelName="Confirm Password :" handleChange={this.handleConfirmPassword}/>
        <Input type="TextArea" labelName="Addres" handleChange={this.handleAddres}/>
        <button type='submit' value="submit" onClick={this.handleCheck} >Submit </button>
        <button type='reset' value="reset">Reset</button>
        </form>
      </div>
      
    )
  }
}

export default FormInput